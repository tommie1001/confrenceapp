const path = require('path');

const express = require('express');
const router = express.Router();

//allUsers
router.get('/', (req, res, next) => {
    const users = ['Rudy', 'Matthijs', 'Stephan'];
    res.render('users/index', {
        path: '/users',
        title: 'All Users',
        description: 'What function does everybody has?',
        users: users,
        add: 'Add User',
        link: 'users/add-user'
    });
});

//AddUser
router.get('/add-user', (req, res, next) => {
    res.render('users/addUser', {
        path: '/add-users',
    });
});


//router.addUser = ();

module.exports = router;